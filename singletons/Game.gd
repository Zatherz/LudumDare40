extends Node2D

signal wave_ended(wave)
signal wave_started(wave)
signal level_started(index)
signal player_died

var DEBUG = ProjectSettings.get("application/config/debug")

var last_source_of_damage # : DamageSource

var current_boss setget set_current_boss, get_current_boss

var already_died_on_level1_boss = false
var knows_about_level1_boss_mechanic = false

func set_current_boss(source):
	if source == null:
		current_boss = null
		return
	current_boss = weakref(source)

func has_current_boss():
	return current_boss != null and current_boss.get_ref() != null

func get_current_boss():
	if current_boss != null:
		return current_boss.get_ref()
	return null

var camera
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	camera = Camera2D.new()
	camera.add_to_group("dont_destroy_on_load")
	camera.position = get_viewport_rect().size/2
	camera.make_current()
	get_tree().get_root().call_deferred("add_child", camera)
	randomize()

var cached_player = null
func get_player():
	var ref
	if cached_player == null: ref = null
	else: ref = cached_player.get_ref()
	if ref: return ref
	var players = get_tree().get_nodes_in_group("player")
	if players.size() == 0: return null
	cached_player = weakref(players[0])
	return players[0]
	
func add_root_child(child):
	RootEntityContainer.add_child(child)
	
func spawn(child, pos):
	if child is PackedScene: child = child.instance()
	if typeof(pos) == TYPE_OBJECT and pos is Position2D: pos = pos.global_position
	child.position = pos
	add_root_child(child)
	return child
	
class DamageSource:
	enum TYPE {UNKNOWN, ENEMY, PROJECTILE, PICKUP}
	enum ORIGIN_TYPE {UNKNOWN, N_A, ENEMY, PLAYER, BOSS}
	enum SUB_TYPE {NONE, BEAM, BOSS}
	
	var name
	var type
	var sub_type
	var origin_type
	static func create_from(source):
		var name = "Unknown"
		var type = TYPE.UNKNOWN
		var origin_type = ORIGIN_TYPE.UNKNOWN
		var sub_type = SUB_TYPE.NONE
		
		name = source.get_name()
		if source.is_in_group("pickup"):
			type = TYPE.PICKUP
		if source.is_in_group("evil"):
			type = TYPE.ENEMY
			origin_type = ORIGIN_TYPE.N_A
			if source.is_in_group("boss"):
				sub_type = SUB_TYPE.BOSS
		elif source.is_in_group("projectile"):
			type = TYPE.PROJECTILE
			if source.is_in_group("projectile:beam"):
				type = SUB_TYPE.BEAM
			if source.source_type == Game.PROJECTILE.SOURCE_TYPE.ENEMY:
				origin_type = ORIGIN_TYPE.ENEMY
			elif source.source_type == Game.PROJECTILE.SOURCE_TYPE.BOSS:
				origin_type = ORIGIN_TYPE.BOSS
			elif source.source_type == Game.PROJECTILE.SOURCE_TYPE.PLAYER:
				origin_type = ORIGIN_TYPE.PLAYER
				
		var src = Game.DamageSource.new()
		src.name = name
		src.type = type
		src.origin_type = origin_type
		src.sub_type = sub_type
		return src
	
const PICKUP_DISPENSER = preload("res://scenes/PickupDispenser.tscn")

const PROJECTILE = preload("res://src/Projectile.gd")

const LEVEL = preload("res://src/Level.gd")

const PROJECTILES = {
	BASIC = preload("res://scenes/Projectile.tscn"),
	BLASTER = preload("res://scenes/Projectiles/BlasterProjectile.tscn"),
	ROCKET = preload("res://scenes/Projectiles/Rocket.tscn"),
	BEAM = preload("res://scenes/Projectiles/Beam.tscn")
}
	
const ENEMIES = {
	DUMBO = preload("res://scenes/Enemies/Dumbo.tscn"),
	UGABUGA = preload("res://scenes/Enemies/Ugabuga.tscn"),
	SUISQUID = preload("res://scenes/Enemies/Suisquid.tscn"),
	PIRATESHIP = preload("res://scenes/Enemies/PirateShip.tscn"),
	BEATLE = preload("res://scenes/Enemies/Beatle.tscn"),
	MEGADUMBO = preload("res://scenes/Enemies/MegaDumbo.tscn"),
	BOMBER = preload("res://scenes/Enemies/Bomber.tscn")
}

const PICKUPS = {
	# Perks
	SHOT_SPEED_PERK = preload("res://scenes/Pickups/ShotSpeedPerk.tscn"),
	DAMAGE_PARK = preload("res://scenes/Pickups/DamagePerk.tscn"),
	BLASTER_PERK = preload("res://scenes/Pickups/BlasterPerk.tscn"),
	FIRERATE_PERK = preload("res://scenes/Pickups/FireratePickup.tscn"),
	
	# Gems
	#HEALTH_GEM = preload("res://scenes/Pickups/HealthGem.tscn"),
	#MAX_HEALTH_GEM = preload("res://scenes/Pickups/MaxHealthGem.tscn"),
}

const SHIELD_REGEN_GEM = preload("res://scenes/Pickups/ShieldRegenGem.tscn")

const CRATES = {
	RED_CRATE = preload("res://scenes/Pickups/RedCrate.tscn")
}

const ABILITY_PICKUPS = {
	SHIELD_RECHARGE = preload("res://scenes/Pickups/ShieldRechargeAbilityPickup.tscn")
}

const LEVELS = {
	1: preload("res://scenes/Levels/Level1.tscn"),
	2: preload("res://scenes/Levels/Level2.tscn")
}

var PICKUPS_ARY = PICKUPS.values()
var CRATES_ARY = CRATES.values()
var ABILITY_PICKUPS_ARY = ABILITY_PICKUPS.values()

const EXPLOSION_EFFECT = preload("res://scenes/ExplosionEffect.tscn")

const ROMAN_NUMERALS = [
	['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'], # ones
	['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'], # tens
	['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'], # hundreds
	['', 'M', 'MM', 'MMM'] # thousands
]

func to_roman(val):
	if val == 0: return "0"
	if val > 3999: return str(val)
	
	# split integer string into array and reverse array
	var strval = str(val)
	var ary = []
	var length = strval.length()
	for i in range(0, length):
		var j = length - i - 1
		ary.append(strval[j])
	var roman_numeral = ''
	
	var i = length;
	while true:
		i -= 1
		if (i < 0): break
		roman_numeral += ROMAN_NUMERALS[i][int(ary[i])] 

	return roman_numeral;

var screen_shake_factor = 0
var screen_shake_time = 0

func apply_screenshake(time, factor):
	if factor >= screen_shake_factor:
		screen_shake_factor = factor
	screen_shake_time = time

onready var fullscreen = ProjectSettings.get("display/window/size/fullscreen")
func _process(delta):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		fullscreen = not fullscreen
		OS.set_window_fullscreen(fullscreen)
	if screen_shake_factor > 0:
		screen_shake_time -= delta
		if screen_shake_time <= 0:
			screen_shake_factor = 0
			screen_shake_time = 0
			camera.offset = Vector2()
			return
		 
		if screen_shake_factor != 0:
			camera.offset = Vector2(
				rand_range(-screen_shake_factor, screen_shake_factor),
				rand_range(-screen_shake_factor, screen_shake_factor)
			)
	else:
		camera.offset = Vector2()
		

func get_enemies():
	return get_tree().get_nodes_in_group("evil")
	
func get_enemy_count():
	return get_tree().get_nodes_in_group("evil").size()

func sprite_width(sprite):
	return sprite.texture.get_width() * sprite.scale.x

func sprite_height(sprite):
	return sprite.texture.get_height() * sprite.scale.y

var wave_from_previous_run = 0
var wave = 0
var random_wave = false
var boss_wave = false

func current_wave_is_a_random_wave():
	random_wave = true
	
func current_wave_is_a_boss_wave():
	boss_wave = true

func end_wave():
	emit_signal("wave_ended", wave)
func start_wave():
	random_wave = false
	boss_wave = false
	wave += 1
	wave_from_previous_run = wave
	emit_signal("wave_started", wave)

func level_ended(): pass

func clear():
	for child in RootEntityContainer.get_children():
		print(child.get_name())
		child.queue_free()
		
	for child in get_tree().get_root().get_children():
		if child.is_in_group("dont_destroy_on_load"): continue
		
		child.queue_free()

const TIPS = [
	"Learning how to effectively use Slow Down is the key to success.",
	"Pickups aren't necessarily your enemies, but they aren't friends either.",
	"Watch out for pickups!",
	"Shooty shooty",
	"This game was made in 72 hours.",
	"This game was made with only free open source software!",
	"Watch out for the red ring."
]

func random_tip():
	return TIPS[randi() % TIPS.size()]

func get_level_designation():
	var s = str("L", current_level, ":", wave_from_previous_run)
	if random_wave: s = str(s, "r")
	if boss_wave: s = str(s, "b")
	return s

func kill_everything_except_bosses():
	var enemies = get_tree().get_nodes_in_group("evil")
	for enemy in enemies:
		if enemy.is_in_group("boss"): continue
		enemy.queue_free()
		
	var pickups = get_tree().get_nodes_in_group("pickup")
	for pickup in pickups: pickup.queue_free()
	
	var projs = get_tree().get_nodes_in_group("projectile")
	for proj in projs: proj.queue_free()

func load_scene_immediate(scene):
	clear()
	wave_from_previous_run = wave
	wave = 0
	var instance = scene.instance()
	if instance is LEVEL:
		emit_signal("level_started", instance.index)
	get_tree().get_root().add_child(instance)

var current_level = 1
func load_level(index):
	current_level = index
	load_scene(LEVELS[index])

var next_scene
func load_scene(scene):
	next_scene = scene
	$Animation.play("nextlevel")
	
func _load_next_scene():
	if next_scene == null:
		print("ERROR: Tried to load a scene with animation but next_scene is null")
		return
	var sc = next_scene
	next_scene = null
	load_scene_immediate(sc)

func restart(): load_level(current_level)
func game_over():
	emit_signal("player_died")
	load_scene(preload("res://scenes/GameOver.tscn"))

func explode(global_pos, length):
	var inst = EXPLOSION_EFFECT.instance()
	inst.length = length
	inst.position = global_pos
	add_root_child(inst)

func explode_without_audio(global_pos, length):
	var inst = EXPLOSION_EFFECT.instance()
	inst.length = length
	inst.position = global_pos
	inst.audio = false
	add_root_child(inst)
	
func explode_scaled(global_pos, length, xscale):
	GlobalAudio.EXPLOSION.play()
	var inst = EXPLOSION_EFFECT.instance()
	inst.length = length
	inst.position = global_pos
	inst.scale = xscale
	add_root_child(inst)

#func connect_difficulty_changed(object, func_name):
#	connect("difficulty_changed", object, func_name)
#	object.call(func_name, difficulty)

func random_pickup():
	return PICKUPS_ARY[randi() % PICKUPS_ARY.size()]
	
func random_crate():
	return CRATES_ARY[randi() % CRATES_ARY.size()]
	
func random_ability_pickup():
	return ABILITY_PICKUPS_ARY[randi() % ABILITY_PICKUPS_ARY.size()]
