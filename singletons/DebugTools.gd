extends Node2D

func _ready():
	if not Game.DEBUG: queue_free()
	add_to_group("dont_destroy_on_load")

func _process(delta):
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if Input.is_action_just_pressed("ui_page_up"):
		Game.end_wave()
		Game.start_wave()
	elif Input.is_action_just_pressed("ui_page_down"):
		for i in range(0, 10):
			Game.end_wave()
			Game.start_wave()
	elif Input.is_action_just_pressed("debug_get_pickups"):
		for i in range(0, 9999):
			var pickup = Game.random_pickup().instance()
			pickup.good_effect(Game.get_player())
			pickup.bad_effect(Game.get_player())
	elif Input.is_action_pressed("debug_first_level"):
		Game.load_scene(preload("res://scenes/Levels/Level1.tscn"))
	elif Input.is_action_pressed("debug_spawn_random_pickup"):
		var crate = Game.random_crate().instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_dumbo"):
		var crate = Game.ENEMIES.DUMBO.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_ugabuga"):
		var crate = Game.ENEMIES.UGABUGA.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_suisquid"):
		var crate = Game.ENEMIES.SUISQUID.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_pirateship"):
		var crate = Game.ENEMIES.PIRATESHIP.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_beatle"):
		var crate = Game.ENEMIES.BEATLE.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_megadumbo"):
		var crate = Game.ENEMIES.MEGADUMBO.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_bomber"):
		var crate = Game.ENEMIES.BOMBER.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
