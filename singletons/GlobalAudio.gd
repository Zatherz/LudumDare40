extends Node

onready var DEATH = $Death
onready var WEAPON_UPGRADE = $WeaponUpgrade
onready var EXPLOSION = $Explosion
onready var GEM1 = $Gem1
onready var GEM2 = $Gem2
onready var GEM3 = $Gem3
onready var GEM4 = $Gem4
onready var POWERUP1 = $Powerup1
onready var POWERUP2 = $Powerup2
onready var POWERUP3 = $Powerup3

func random_gem():
	var i = floor(rand_range(1, 5))
	
	if i ==1:    return GEM1
	elif i == 2: return GEM2
	elif i == 3: return GEM3
	elif i == 4: return GEM4
	
	return GEM1

func random_powerup():
	var i = floor(rand_range(1, 4))
	
	if i ==1:    return POWERUP1
	elif i == 2: return POWERUP2
	elif i == 3: return POWERUP3
	
	return POWERUP1
