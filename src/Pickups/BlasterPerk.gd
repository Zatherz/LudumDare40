extends "res://src/Pickup.gd"

func good_effect(player):
	player.set_projectile_in_both_turrets(Game.PROJECTILES.BLASTER)
	GlobalAudio.WEAPON_UPGRADE.play()
