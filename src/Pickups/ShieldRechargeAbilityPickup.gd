extends "res://src/Ability.gd"

export(float) var amount = 1

func _ready(): name = "Shield Recharge"

func _time_to_recharge():
	player.shield_juice = min(player.shield_juice + amount, player.max_shield_juice)
