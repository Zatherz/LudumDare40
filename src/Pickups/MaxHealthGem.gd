extends "res://src/Pickup.gd"

export(float) var additional_max_hp = 10

func _ready():
	$Animation.play("shine")

func good_effect(player):
	GlobalAudio.random_gem().play()
	player.max_health += additional_max_hp
	player.health += additional_max_hp