extends "res://src/Pickup.gd"

export(float) var additional_shot_speed = 10

func good_effect(player):
	GlobalAudio.random_powerup().play()
	player.increase_shot_speed_of_both_turrets(additional_shot_speed) 