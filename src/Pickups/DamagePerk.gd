extends "res://src/Pickup.gd"

export(int) var additional_damage = 2

func good_effect(player):
	GlobalAudio.random_powerup().play()
	player.increase_damage_of_both_turrets(additional_damage)
