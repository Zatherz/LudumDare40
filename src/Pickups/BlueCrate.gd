extends "res://src/Pickup.gd"

func _ready(): prevent_deletion_after_activation = true

func pop_open():
	var amount = floor(rand_range(1, 3))
	
	var player = Game.get_player()
	if player:
		var ability = Game.random_ability()
		player.grant_ability(ability)
	
	for i in range(0, 3):
		var pickup = Game.random_pickup()
		var inst = pickup.instance()
		
		var x = floor(rand_range(
			$SpawnBounds/LowerLeft.global_position.x,
			$SpawnBounds/LowerRight.global_position.x
		))
		
		var y = floor(rand_range(
			$SpawnBounds/Top.global_position.y,
			$SpawnBounds/LowerLeft.global_position.y
		))
		
		inst.position = Vector2(x, y)
		
		Game.add_root_child(inst)

func good_effect(player):
	GlobalAudio.random_powerup().play()
	$Tween.interpolate_property(
		self,
		"position",
		position,
		Vector2(position.x, position.y - 100),
		0.25,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	$Tween.start()
	$Animation.play("pop")
	
func bad_effect(player): pass # negate the difficulty increase
