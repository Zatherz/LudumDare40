extends "res://src/Pickup.gd"

var max_life = 1
var life = 0
var popped_open = false

func _ready(): prevent_deletion_after_activation = true

func _process(delta):
	if not popped_open:
		life += delta
	
		if life >= max_life:
			popped_open = true
			$Animation.play("pop")

func pop_open():
	var amount = floor(rand_range(1, 3))
	
	for i in range(0, 3):
		var pickup = Game.random_pickup()
		var inst = pickup.instance()
		
		var x = floor(rand_range(
			$SpawnBounds/LowerLeft.global_position.x,
			$SpawnBounds/LowerRight.global_position.x
		))
		
		var y = floor(rand_range(
			$SpawnBounds/Top.global_position.y,
			$SpawnBounds/LowerLeft.global_position.y
		))
		
		inst.position = Vector2(x, y)
		
		Game.add_root_child(inst)

func good_effect(player):
	if popped_open: return
	GlobalAudio.random_powerup().play()
#	$Tween.interpolate_property(
#		self,
#		"position",
#		position,
#		Vector2(position.x, position.y - 100),
#		0.25,
#		Tween.TRANS_LINEAR,
#		Tween.EASE_IN_OUT
#	)
#	$Tween.start()
	popped_open = true
	$Animation.play("pop")
	
func bad_effect(player): pass # negate the difficulty increase
