extends "res://src/Pickup.gd"

export(float) var wait_time_improvement = 0.025

func good_effect(player):
	GlobalAudio.random_powerup().play()
	var fd = player.get_node("FireDelay")
	fd.wait_time = max(0.05, fd.wait_time - wait_time_improvement)
