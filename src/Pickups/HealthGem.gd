extends "res://src/Pickup.gd"

export(float) var stored_hp = 10

func _ready():
	$Animation.play("shine")

func good_effect(player):
	GlobalAudio.random_gem().play()	
	player.health = min(player.health + stored_hp, player.max_health)