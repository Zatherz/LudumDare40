extends Position2D

export(bool) var moving_horizontally = true
export(float) var speed = 5
export(float) var crate_chance = 0.3
var pickup_list = null
export(float) var spawn_interval = 1 setget set_spawn_interval, get_spawn_interval
export(Vector2) var direction = Vector2(1, 0)
export(bool) var frozen = false

onready var is_ready = true

func update_spawn_interval():
	$SpawnTimer.wait_time = spawn_interval

func set_spawn_interval(val):
	spawn_interval = val
	if is_ready: update_spawn_interval()
	
func get_spawn_interval():
	return spawn_interval

func _ready():
	update_spawn_interval()
	$SpawnTimer.start()

func _physics_process(delta):
	if frozen: return
	
	var viewport_size = get_viewport_rect().size
	var would_be_pos = global_position + direction * speed
	if would_be_pos.x <= 0 or would_be_pos.x >= viewport_size.x:
		direction = -direction
	else:
		global_position = would_be_pos

func _on_SpawnTimer_timeout():
	if frozen: return
	
	var pickup
	if pickup_list == null:
		if randf() <= crate_chance: pickup = Game.random_crate()
		else: pickup = Game.random_pickup()
	else:
		pickup = pickup_list[randi() % pickup_list.size()]
	Game.spawn(pickup, global_position)