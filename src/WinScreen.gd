extends Node2D

func _ready():
	$Music.play()

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		Game.load_scene(preload("res://scenes/TitleScreen.tscn"))
		$Music/Animation.play("fadeout")
