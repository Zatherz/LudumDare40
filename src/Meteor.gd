extends Sprite

export(float) var speed = 3
var direction
export(float) var rotation_speed = 0.1

func _ready():
	if direction == null:
		var x = rand_range(-1, 2)
		var y = rand_range(-1, 2)
		if x >= 0 and x < 0.05: x = 0.05
		if y >= 0 and y < 0.05: y = 0.05
		if x <= 0 and x > -0.05: x = -0.05
		if y <= 0 and y > -0.05: y = -0.05
		direction = Vector2(x, y)

func _physics_process(delta):
	position += direction * speed
	rotation += rotation_speed