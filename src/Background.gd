extends Node2D

const PLANET_SATURN = preload("res://gfx/space-shooter/backgrounds/planet-1.png")
const PLANET_EARTH = preload("res://gfx/space-shooter/backgrounds/planet-2.png")

const YELLOW_STAR = preload("res://gfx/space-shooter/backgrounds/star-1.png")
const RED_STAR = preload("res://gfx/space-shooter/backgrounds/star-2.png")

const METEORS = [
	preload("res://gfx/space-shooter/backgrounds/meteor-1.png"),
	preload("res://gfx/space-shooter/backgrounds/meteor-2.png"),
	preload("res://gfx/space-shooter/backgrounds/meteor-3.png"),
	preload("res://gfx/space-shooter/backgrounds/meteor-4.png"),
	# preload("res://gfx/space-shooter/backgrounds/meteor-5.png"),
	# excluded due to being too small (could be confused with projectiles)
]

const METEOR = preload("res://scenes/Meteor.tscn")

var earth_and_moon_direction
var saturn_direction

var earth_and_moon_scale_factor
var saturn_scale_factor

func _ready():
	earth_and_moon_direction = Vector2(rand_range(-1, 1), rand_range(-1, 1)) * 0.05
	saturn_direction = Vector2(rand_range(-1, 1), rand_range(-1, 1)) * 0.05
	
	var ems = rand_range(0.99999, 1.00001)
	earth_and_moon_scale_factor = Vector2(ems, ems)
	var ss = rand_range(0.99999, 1.00001)
	saturn_scale_factor = Vector2(ss, ss)
	
	for i in range(0, 30): _on_MeteorTimer_timeout()

func _physics_process(delta):
	if Input.is_action_just_pressed("ui_page_down"):
		_on_MeteorTimer_timeout()
	$Earth.position += earth_and_moon_direction
	$Moon.position += earth_and_moon_direction
	$Saturn.position += saturn_direction
	
	$Earth.scale *= earth_and_moon_scale_factor
	$Moon.scale *= earth_and_moon_scale_factor
	$Saturn.scale *= saturn_scale_factor

func _on_MeteorTimer_timeout():
	var meteor = METEOR.instance()
	meteor.texture = METEORS[randi() % METEORS.size()]
	var viewport_size = get_viewport_rect().size
	var x = 0
	var dirx = 0
	if randi() % 2 == 0:
		x = rand_range(-70, 20)
		dirx = rand_range(0.05, 0.5)
	else:
		x = rand_range(viewport_size.x + 20, viewport_size.x + 70)
		dirx = rand_range(-0.5, -0.05)
	var y = 0
	var diry = 0
	if randi() % 2 == 0:
		y = rand_range(-70, 20)
		diry = rand_range(0.05, 0.5)
	else:
		y = rand_range(viewport_size.y + 20, viewport_size.y + 70)
		diry = rand_range(-0.5, -0.05)
	meteor.rotation_speed = rand_range(0.001, 0.01)
	meteor.position = Vector2(x, y)
	meteor.speed = rand_range(0.1, 2)
	meteor.direction = Vector2(dirx, diry)
	add_child(meteor)
