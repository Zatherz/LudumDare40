extends Node2D

func update_title():	
	$Hints/Shift/Label.rect_size = Vector2(1, 1)
	$Hints/Enter/Label.rect_size = Vector2(1, 1)
	$Hints/Space/Label.rect_size = Vector2(1, 1)
	$Hints/WASD/Label.rect_size = Vector2(1, 1)
	
	$Hints/Shift.rect_position.x = get_viewport_rect().size.x/2 - ($Hints/Shift.rect_size.x/2 + $Hints/Shift/Label.rect_size.x/2)
	$Hints/Space.rect_position.x = get_viewport_rect().size.x/2 - ($Hints/Space.rect_size.x/2 + $Hints/Space/Label.rect_size.x/2)
	$Hints/Enter.rect_position.x = get_viewport_rect().size.x/2 - ($Hints/Enter.rect_size.x/2 + $Hints/Enter/Label.rect_size.x/2)
	$Hints/WASD.rect_position.x = get_viewport_rect().size.x/2 - ($Hints/WASD.rect_size.x/2 + $Hints/WASD/Label.rect_size.x/2)
	
func _ready():
	$Music.play()
	$StartLabel.rect_size = Vector2()
	$StartLabel.rect_position.x = get_viewport_rect().size.x/2 - $StartLabel.rect_size.x/2
	update_title()
	
func _process(delta):
	if Input.is_action_just_released("ui_accept"):
		$Music/Animation.play("fadeout")
		Game.current_level = 1
		Game.restart()