extends "res://src/Ability.gd"

export(float) var amount = 0.001

func _time_to_recharge():
	if player.shielding: return
	player.shield_juice = min(player.shield_juice + amount, player.max_shield_juice)