extends Node2D

var wave = 0
var subwave = 0

func summon(scene, pos):
	var inst = scene.instance()
	inst.position = pos
	Game.add_root_child(inst)
	
func summon_dumbo(pos, force_drop_chance):
	var inst = Game.ENEMIES.DUMBO.instance()
	inst.position = pos
	inst.forced_pickup_drop_chance = force_drop_chance
	Game.add_root_child(inst)
	
func summon_suisquid(pos):
	var inst = Game.ENEMIES.SUISQUID.instance()
	inst.position = pos
	Game.add_root_child(inst)

func summon_wave(n):
	print("WAVE ", n)
	if n == 0:
		summon_dumbo($SummonPoints/TopCenter.global_position, 0)
	elif n == 1:
		summon_dumbo($SummonPoints/TopCenter.global_position - Vector2(50, 0), 0)
		summon_dumbo($SummonPoints/TopCenter.global_position + Vector2(50, 0), 0)
	elif n == 2:
		summon_dumbo($SummonPoints/TopCenter.global_position, 1)
	elif n == 3:
		for i in range(0, 4):
			var x = rand_range($SummonPoints/TopLeft.global_position.x, $SummonPoints/TopRight.global_position.x)
			var pos = Vector2(x, $SummonPoints/TopLeft.global_position.y)
			summon(Game.ENEMIES.DUMBO, pos)
	elif n == 4:
		for i in range(0, 8):
			var x = rand_range($SummonPoints/TopLeft.global_position.x, $SummonPoints/TopRight.global_position.x)
			var pos = Vector2(x, $SummonPoints/TopLeft.global_position.y)
			summon(Game.ENEMIES.DUMBO, pos)
	elif n == 5:
		for i in range(0, 12):
			var x = rand_range($SummonPoints/TopLeft.global_position.x, $SummonPoints/TopRight.global_position.x)
			var pos = Vector2(x, $SummonPoints/TopLeft.global_position.y)
			summon(Game.ENEMIES.DUMBO, pos)
	elif n == 6:
		summon_suisquid($SummonPoints/TopCenter.global_position)
	elif n == 7:
		for i in range(0, 20):
			var x = rand_range($SummonPoints/TopLeft.global_position.x, $SummonPoints/TopRight.global_position.x)
			var pos = Vector2(x, $SummonPoints/TopLeft.global_position.y)
			summon_suisquid(pos)
	elif n == 8:
		subwave = 0
		for i in range(0, 4):
			var x = rand_range($SummonPoints/TopLeft.global_position.x, $SummonPoints/TopRight.global_position.x)
			var pos = Vector2(x, $SummonPoints/TopLeft.global_position.y)
			if randi() % 2 == 0: summon_suisquid(pos)
			else: summon_dumbo(pos, null)
	else:
		restart() # this will load up Game.tscn

func _ready():
	print("The game has begun")
	call_deferred("begin")

func begin():
	Game.connect("difficulty_changed", self, "_difficulty_changed")
	summon_wave(0)
	$EnemyCountTestInterval.start()
	
func _process(delta):
	get_child_count()
	
func _difficulty_changed(diff):
	pass

func _check_enemy_count():
	var enemies = get_tree().get_nodes_in_group("evil")
	if enemies.size() == 0:
		print("All the enemies are dead. proceeding")
		wave += 1
		summon_wave(wave)

func _on_SpawnTimer_timeout():
	if wave == 8:
		if subwave % 4 == 0: # every 4 seconds
			for i in range(0, floor(rand_range(1, 3))):
				var x = rand_range($SummonPoints/TopLeft.global_position.x, $SummonPoints/TopRight.global_position.x)
				var pos = Vector2(x, $SummonPoints/TopLeft.global_position.y)
				if randi() % 2 == 0: summon_suisquid(pos)
				else: summon_dumbo(pos, null)
		subwave += 1
