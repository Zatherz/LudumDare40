extends Area2D

const FALLING_SPEED = 2

var prevent_deletion_after_activation = false
var disable_bad_effect = false
var frozen = false

func _ready():
	if not is_in_group("pickup"): add_to_group("pickup")

func good_effect(player): pass
func bad_effect(player):
	if Game.DEBUG and Input.is_key_pressed(KEY_SPACE):
		return
	player.pickup_decrease_health(3, self)
	
func do_good_effect(player): good_effect(player)
func do_bad_effect(player): if not disable_bad_effect: bad_effect(player)

func _physics_process(delta):
	if frozen: return
	position.y += FALLING_SPEED
	if position.y > get_viewport_rect().size.y + $Sprite.texture.get_height() * $Sprite.scale.y:
		queue_free()

func _body_entered(body):
	if frozen: return
	if body.is_in_group("player"):
		body.activate_pickup(self)
		if not prevent_deletion_after_activation:
			queue_free()