extends "res://src/Projectile.gd"

var target = null
export(float) var speed_once_activated = 8
export(int) var bullets_to_explode_into = 18

const SPAWNED_BULLETS_DISTANCE_FROM_ORIGIN_FACTOR = 50

func _ready():
	dont_destroy_when_not_on_screen = true

func explode_into_bullets():
	$Explosion.play()

	for i in range(0, bullets_to_explode_into):
		var rad = i * PI / (bullets_to_explode_into / 2)
		var dir = Vector2(sin(rad), cos(rad))
		var proj = Game.PROJECTILES.BLASTER.instance()
		proj.position = global_position + dir * SPAWNED_BULLETS_DISTANCE_FROM_ORIGIN_FACTOR
		proj.direction = dir
		proj.lifetime = 500
		proj.rotation = dir.angle() + PI/2
		proj.excluded_groups = excluded_groups
		proj.excluded_nodes = excluded_nodes
		proj.damage = damage
		Game.add_root_child(proj)
		Game.explode_without_audio(global_position + dir * 50, 0.5)
		
func on_body_impact(body):
	Game.explode(global_position, 0.5)
	Game.apply_screenshake(1, 4)
	explode_into_bullets()
		
	.on_body_impact(body)

var already_exploded_from_crosshair_touch = false
var chance_to_drop_pickup = 0.5

func on_impact(body):
	.on_impact(body)
	if body.is_in_group("enemy:pirateship/crosshair"):
		if not already_exploded_from_crosshair_touch:
			body.start_decay()
			already_exploded_from_crosshair_touch = true
			if randf() < chance_to_drop_pickup:
				var pickup = Game.random_pickup().instance()
				pickup.position = global_position
				Game.add_root_child(pickup)
			Game.apply_screenshake(0.4, 3)
			explode_into_bullets()

var idle_time_ended = false

func _idle_time_ended():
	idle_time_ended = true
	if target == null or target.get_ref() == null: return
	var pos = target.get_ref().global_position
	var dir = (pos - global_position).normalized()
	direction = dir
	lifetime = 20
	rotation = dir.angle() + PI/2
	speed = speed_once_activated
