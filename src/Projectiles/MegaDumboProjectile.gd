extends "res://src/Projectile.gd"

func _ready():
	if not is_in_group("enemy:megadumbo/projectile"): add_to_group("enemy:megadumbo/projectile")

func on_body_impact(body):
	if body.is_in_group("player") and body.shielding:
		Game.knows_about_level1_boss_mechanic = true
		direction = -direction
		rotation = -rotation
		excluded_groups = ["player"]
		damage = 1
		$Tween.stop(self, "damage")
		$Tween.reset($Sprite, "modulate")
		return true # to prevent the projectile from being destroyed
	.on_body_impact(body)