extends "res://src/Projectile.gd"

#@deprecated
var origin # : Node

var origin_pos
export(Color) var color = Color(1, 0, 0)
export(int) var thickness = 10
export(bool) var antialiasing_enabled = true

func _ready():
	dont_destroy_when_not_on_screen = true
	if not is_in_group("projectile:beam"): add_to_group("projectile:beam")
	if not is_in_group("projectile_pierces_shield"): add_to_group("projectile_pierces_shield")
	rotation = 0

func get_origin_pos():
	if origin_pos != null: return origin_pos
	return origin.get_ref().global_position

func _draw():
	var inv = get_global_transform().inverse()
	draw_set_transform(inv.get_origin(), inv.get_rotation(), inv.get_scale())

	draw_line(get_origin_pos(), global_position, Color(color.r, color.g, color.b, $Sprite.modulate.a), thickness, antialiasing_enabled)

func _process(delta):
	if origin.get_ref() == null:
		queue_free()
		return
	
	update()
	
	if $Sprite.modulate.a >= IF_ALPHA_IS_LOWER_THAN_THIS_DONT_DEAL_DAMAGE:
		$RayCast.cast_to = get_origin_pos() - global_position
		$RayCast.force_raycast_update()
		if $RayCast.is_colliding():
			var obj = $RayCast.get_collider()
			if obj.is_in_group("player"):
				Game.apply_screenshake(0.1, 4)
				obj.take_damage(damage, self)
