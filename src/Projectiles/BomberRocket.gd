extends "res://src/Projectile.gd"

const SPAWNED_BULLETS_DISTANCE_FROM_ORIGIN_FACTOR = 50
export(int) var bullets_to_explode_into = 6
var exploded = false

func explode_into_bullets():
	if exploded: return
	exploded = true
	$Explosion.play()

	for i in range(0, bullets_to_explode_into):
		var rad = i * PI / (bullets_to_explode_into / 2)
		var dir = Vector2(sin(rad), cos(rad))
		var proj = Game.PROJECTILES.BLASTER.instance()
		proj.position = global_position + dir * SPAWNED_BULLETS_DISTANCE_FROM_ORIGIN_FACTOR
		proj.direction = dir
		proj.lifetime = 500
		proj.rotation = dir.angle() + PI/2
		proj.excluded_groups = excluded_groups
		proj.excluded_nodes = excluded_nodes
		Game.add_root_child(proj)
		Game.explode_without_audio(global_position + dir * 50, 0.5)

func _physics_process(delta):
	speed += 1
	
	if global_position.y >= get_viewport_rect().size.y:
		explode_into_bullets()

func _on_Explosion_finished():
	queue_free()
