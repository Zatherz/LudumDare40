extends "res://src/Level.gd"

var wave_range = [1, 11]

func _ready():
	Game.current_level = 2 # in case this level is started directly

func random_side_spawn_point():
	if randi() % 2 == 0: return $LeftSpawnPoint.global_position
	else: return $RightSpawnPoint.global_position
	
func random_top_spawn_point_for(inst):
	var sprite_width = Game.sprite_width(inst.get_node("Sprite"))
	var x = rand_range(sprite_width/2, get_viewport_rect().size.x - sprite_width/2)
	var y = rand_range($TopSpawnBoundary.global_position.y, $BottomSpawnBoundary.global_position.y)
	print("pos ", x, ", ", y)
	return Vector2(x, y)

func random_wave(hard):
	var dumbo_chance = 0.8
	var beatle_chance = 0.5
	var double_beatle_chance = 0.2
	var ugabuga_chance = 0.4
	var suisquid_chance = 0.6
	var dumbo_max = 16
	var dumbo_failsafe_max = 6
	var suisquid_max = 6
	
	if hard:
		dumbo_chance = 1
		beatle_chance = 0.6
		double_beatle_chance = 0.3
		ugabuga_chance = 0.6
		suisquid_chance = 0.8
		dumbo_max = 20
		dumbo_failsafe_max = 20
		suisquid_max = 8
	
	var spawned_something = false
	if randf() < 0.8:
		spawned_something = true
		for i in range(0, floor(rand_range(1, 16))):
			var inst = Game.ENEMIES.DUMBO.instance()
			Game.spawn(inst, random_top_spawn_point_for(inst))
	if randf() < 0.5:
		spawned_something = true
		if randf() < 0.2:
			Game.spawn(Game.ENEMIES.BEATLE, $BottomSpawnBoundary.global_position + Vector2(-100, 0))
			Game.spawn(Game.ENEMIES.BEATLE, $BottomSpawnBoundary.global_position + Vector2(100, 0))
		else:
			Game.spawn(Game.ENEMIES.BEATLE, $BottomSpawnBoundary)
	if randf() < 0.4:
		spawned_something = true
		Game.spawn(Game.ENEMIES.UGABUGA, random_side_spawn_point())
	if randf() < 0.6:
		spawned_something = true
		for i in range(0, floor(rand_range(0, 6))):
			var inst = Game.ENEMIES.SUISQUID.instance()
			Game.spawn(inst, random_top_spawn_point_for(inst))
	if not spawned_something:
		Game.spawn(Game.ENEMIES.UGABUGA, random_side_spawn_point())
		for i in range(0, floor(rand_range(1, 6))):
			var inst = Game.ENEMIES.DUMBO.instance()
			Game.spawn(inst, random_top_spawn_point_for(inst))

func wave(n):
	if n == 1:
		for i in range(0, 10):
			var inst = Game.ENEMIES.DUMBO.instance()
			Game.spawn(inst, random_top_spawn_point_for(inst))
		Game.spawn(Game.ENEMIES.BEATLE, $BottomSpawnBoundary)
	elif n == 2:
		Game.spawn(Game.PICKUP_DISPENSER, Vector2(0, 0))
		var dis2 = Game.PICKUP_DISPENSER.instance()
		dis2.direction = Vector2(-1, 0)
		Game.spawn(dis2, Vector2(get_viewport_rect().size.x, 0))
		
		Game.spawn(Game.ENEMIES.BEATLE, $BottomSpawnBoundary.global_position + Vector2(100, 0))
		Game.spawn(Game.ENEMIES.BEATLE, $BottomSpawnBoundary.global_position + Vector2(-100, 0))
	elif n == 3:
		Game.spawn(Game.ENEMIES.UGABUGA, random_side_spawn_point())
	elif n == 4:
		Game.spawn(Game.ENEMIES.BEATLE, $BottomSpawnBoundary)
		Game.spawn(Game.ENEMIES.UGABUGA, random_side_spawn_point())
	elif 5 <= n and n <= 10:
		Game.current_wave_is_a_random_wave()
		random_wave(false)
	elif n == 11:
		Game.spawn(Game.ENEMIES.UGABUGA, $LeftSpawnPoint)
		var uga2 = Game.ENEMIES.UGABUGA.instance()
		uga2.switch_order = true
		Game.spawn(uga2, $RightSpawnPoint)
	elif n == 12:
		Game.kill_everything_except_bosses()
		for dispenser in get_tree().get_nodes_in_group("pickup_dispenser"): dispenser.queue_free()
		Game.current_boss = Game.spawn(Game.ENEMIES.PIRATESHIP, $TopSpawnBoundary)

func pre_wave(last_wave):
	if last_wave == 11 and $TimerUntilBossWave.is_stopped():
		$TimerUntilBossWave.start()
		
func all_enemies_are_dead(wave):
	if wave == 0: return
	if wave_range[0] <= wave and wave <= wave_range[1]:
		end_wave()
	if wave_range[0] <= wave and wave <= wave_range[1] - 1:
		print("STARTING ", wave)
		start_wave()
		
	if wave >= 12: Game.load_scene(preload("res://scenes/WinScreen.tscn"))

func _on_TimerUntilFirstWave_timeout():
	print("Start wave")
	start_wave()


func _on_TimerUntilBossWave_timeout():
	start_wave()
