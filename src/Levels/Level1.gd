extends "res://src/Level.gd"
	
var max_wave_4_spawns = 15
var boss
var dispenser

var play_boss_hint_on_boss_ready = false
var doing_boss_hint = false

func _boss_ready():
	if play_boss_hint_on_boss_ready: $Animation.play("boss_hint")
	else: $TimerUntilBossHint.start()
	
func _process(delta):
	if Game.knows_about_level1_boss_mechanic and not $TimerUntilBossHint.is_stopped():
		print("boss hint canceled")
		$TimerUntilBossHint.stop()

func do_boss_hint():
	if dispenser and dispenser.get_ref():
		dispenser.get_ref().frozen = true
	$Player.can_move = false
	boss.get_ref().fake_damage = true
	boss.get_ref().frozen = true
	Game.kill_everything_except_bosses()
	
	var shield_regen_gem = Game.spawn(Game.SHIELD_REGEN_GEM, $ShieldRegenGemPosition)
	shield_regen_gem.frozen = true
	
	$BossHintTween.interpolate_property(
		$Player,
		"position",
		$Player.position,
		$BossHintPosition.position,
		2,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	
	$BossHintTween.interpolate_property(
		shield_regen_gem,
		"position",
		shield_regen_gem.position,
		$BossHintPosition.position,
		2,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	
	$BossHintTween.start()
	

func _on_BossHintTween_tween_completed(object, key):
	if object == $Player:
		$CinematicExperience/Enter.texture = preload("res://gfx/enterpressed.png")
		$Player.shielding = true
		for i in range(0,8): boss.get_ref().fire_octo()
		$BossHintShieldTimer.start()
	else:
		object.queue_free()

func _on_BossHintShieldTimer_timeout():
	$Player.can_move = true
	$Player.shielding = false
	$BossUnfreezeTimer.start()
	$Animation.play("boss_hint_inverse")

func _on_BossUnfreezeTimer_timeout():
	if boss and boss.get_ref():
		boss.get_ref().fake_damage = false
		boss.get_ref().frozen = false
	if dispenser and dispenser.get_ref():
		dispenser.get_ref().frozen = false

func _player_died():
	if boss != null and boss.get_ref() != null:
		Game.already_died_on_level1_boss = true

func _ready():
	Game.connect("player_died", self, "_player_died")
	print("LEVEL ONE")
	start_wave()
	
func wave(n):
	var viewport_size = get_viewport_rect().size
	
	if n == 1:
		var inst = Game.ENEMIES.DUMBO.instance()
		inst.target_y_level = viewport_size.y/2
		Game.spawn(inst, $CenterSpawnPoint)
	elif n == 2:
		for i in range(0, 10):
			Game.spawn(Game.ENEMIES.DUMBO, Vector2(rand_range(0, viewport_size.x), $CenterSpawnPoint.position.y))
	elif n == 3:
		for i in range(0, 5):
			var inst = Game.ENEMIES.DUMBO.instance()
			inst.health *= 2
			Game.spawn(inst, Vector2(rand_range(0, viewport_size.x), $CenterSpawnPoint.position.y - 50))
		for i in range(0, 10):
			Game.spawn(Game.ENEMIES.DUMBO, Vector2(rand_range(0, viewport_size.x), $CenterSpawnPoint.position.y))
	elif n == 4:
		$Wave4Ticker.start()
		_wave_4_tick()
	elif n == 5:
		for i in range(0, 5):
			var inst1 = Game.ENEMIES.DUMBO.instance()
			inst1.health *= 3
			inst1.get_node("FireDelay").wait_time /= 3.0
			Game.spawn(inst1, Vector2(Game.sprite_width(inst1.get_node("Sprite")), -(i * 50)))
			
			var inst2 = Game.ENEMIES.DUMBO.instance()
			inst2.health *= 3
			inst2.get_node("FireDelay").wait_time /= 3.0
			Game.spawn(inst2, Vector2(viewport_size.x - Game.sprite_width(inst2.get_node("Sprite")), -(i * 50)))
	elif n == 6:
		for i in range(0, 5):
			var inst = Game.ENEMIES.SUISQUID.instance()
			inst.force_multiplier = Vector2(1, 2)
			inst.minimum_y_velocity = 2
			Game.spawn(inst, $CenterSpawnPoint.position - Vector2(0, i * 40))
		for i in range(0, 5):
			var inst1 = Game.ENEMIES.DUMBO.instance()
			inst1.health *= 2
			inst1.get_node("FireDelay").wait_time = 0.1
			Game.spawn(inst1, Vector2(Game.sprite_width(inst1.get_node("Sprite")), -(i * 50)))
			
			var inst2 = Game.ENEMIES.DUMBO.instance()
			inst2.health *= 2
			inst2.get_node("FireDelay").wait_time = 0.1
			Game.spawn(inst2, Vector2(viewport_size.x - Game.sprite_width(inst2.get_node("Sprite")), -(i * 50)))
	elif n == 7:
		dispenser = weakref(Game.PICKUP_DISPENSER.instance())
		Game.spawn(dispenser.get_ref(), $CenterSpawnPoint)
		for i in range(0, 10):
			var inst = Game.ENEMIES.SUISQUID.instance()
			inst.force_multiplier = Vector2(1, 2.5)
			inst.minimum_y_velocity = 2.5
			var sprite_width = Game.sprite_width(inst.get_node("Sprite"))
			var x = rand_range(sprite_width, get_viewport_rect().size.x - sprite_width)
			var y = rand_range(-30, -250)
			Game.spawn(inst, Vector2(x, y))
	elif n == 8:
		boss = weakref(Game.spawn(Game.ENEMIES.MEGADUMBO, $MegaDumboSpawnPoint))
		boss.get_ref().connect("in_position", self, "_boss_ready")
		Game.current_boss = boss.get_ref()
		if Game.already_died_on_level1_boss and not Game.knows_about_level1_boss_mechanic:
			play_boss_hint_on_boss_ready = true

func pre_wave(last_wave):
	print("pre wave last ", last_wave)
	if last_wave == 4 and $Wave5Delay.is_stopped():
		print("yes")
		$Wave4Ticker.stop()
		$Wave5Delay.start()
	elif last_wave == 8 and $TimerUntilNextLevel.is_stopped():
		$TimerUntilNextLevel.start()

func all_enemies_are_dead(n):
	if 1 <= n and n <= 8:
		end_wave()
	if 1 <= n and n <= 3 or 5 <= n and n <= 7: 
		start_wave()

var wave_4_spawns = 0
func _wave_4_tick():
	wave_4_spawns += 1
	var viewport_size = get_viewport_rect().size
	
	var inst = Game.ENEMIES.DUMBO.instance()
	inst.health *= 2
	Game.spawn(inst, Vector2(rand_range(0, viewport_size.x), $CenterSpawnPoint.position.y - 50))
	if wave_4_spawns >= max_wave_4_spawns:
		print("Tick wave 4")
		end_wave()
		start_wave()

func _start_wave_5():
	print("Start wave 5")
	start_wave()

func _on_TimerUntilBossHint_timeout():
	$Animation.play("boss_hint")

func _on_TimerUntilNextLevel_timeout():
	Game.load_level(2)
