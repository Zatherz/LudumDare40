extends Node2D

var force_hide = false

func force_hide():
	force_hide = true
	
func unhide():
	force_hide = false

func _wave_started(wave):
	$WaveLabel.text = Game.to_roman(wave)
	$WaveLabel.rect_position = Vector2(
		get_viewport_rect().size.x/2 - $WaveLabel.rect_size.x/2,
		$WaveLabel.rect_position.y
	)
	$WaveLabel/Animation.play("popup")

func _level_started(level):
	$Tip.text = Game.random_tip()
	$Tip/Shade.text = $Tip.text
	$Tip/Animation.play("popup")
	
	$LevelLabel.rect_size = Vector2(1, 1)
	print("LVL START ", level)
	$LevelLabel.text = str("LEVEL ", Game.to_roman(level))
	$LevelLabel/Shade.text = $LevelLabel.text
	
	$LevelLabel.rect_position = Vector2(
		get_viewport_rect().size.x/2 - $LevelLabel.rect_size.x/2,
		$LevelLabel.rect_position.y
	)
	$LevelLabel/Animation.play("popup")

func _ready():
	if get_parent() is Game.LEVEL:
		_level_started(get_parent().index)
	Game.connect("wave_started", self, "_wave_started")
	Game.connect("level_started", self, "_level_started")

func _process(delta):
	var player = Game.get_player()
	for child in $Abilities.get_children():
		child.queue_free()
	if player and not force_hide:
		show()
		
		if Game.has_current_boss():
			$BossHealthBar.show()
			$BossHealthBar.set_max_value(Game.current_boss.get_max_health())
			$BossHealthBar.set_value(Game.current_boss.get_health())
		else: $BossHealthBar.hide()
		
		$HealthBarBackground/HealthBar.max_value = player.max_health
		$HealthBarBackground/HealthBar.value = player.health
		
		$ShieldBarBackground/ShieldBar.max_value = player.max_shield_juice
		$ShieldBarBackground/ShieldBar.value = player.shield_juice
		
		for ability in player.get_abilities():
			var tex = TextureRect.new()
			tex.texture = ability.icon
			$Abilities.add_child(tex)
			
		$Abilities.rect_position.x = get_viewport_rect().size.x - $Abilities.rect_size.x/2
	else:
		hide()