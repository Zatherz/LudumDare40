extends Node2D

func _ready():
	$DifficultyLabel.text = str("Killed by ", get_death_reason(), ".\nLEVEL REACHED: ", Game.get_level_designation())
	$Music.play()
	
func get_death_reason():
	if Game.last_source_of_damage != null:
		var source = Game.last_source_of_damage
		
		var is_boss = (
			(source.type == Game.DamageSource.TYPE.ENEMY and source.sub_type == Game.DamageSource.SUB_TYPE.BOSS) or
			(source.type == Game.DamageSource.TYPE.PROJECTILE and source.origin_type == Game.DamageSource.ORIGIN_TYPE.BOSS)
		)
		
		if source.type == Game.DamageSource.TYPE.PICKUP:
			return "greed"
		elif is_boss:
			return "unmatched strength"
	return "bad reflexes"

func _process(delta):
	if Input.is_action_just_pressed("restart"):
		$Music/Animation.play("fadeout")
		Game.restart()
