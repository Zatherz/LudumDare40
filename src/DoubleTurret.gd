extends Position2D

export(Vector2) var first_direction = Vector2(-0.1, 1)
export(Vector2) var second_direction = Vector2(0.1, 1)
export(PackedScene) var projectile = null
export(float) var projectile_speed = 10
export(int) var projectile_damage = 10
export(float) var projectile_lifetime = 3
export(Array, String) var excluded_groups = []
export(float) var homing_factor = 0
export(bool) var use_sfx = true

func fire():
	if use_sfx: $Laser.play()
	Game.apply_screenshake(0.05, 0.5)	
	var left_proj = projectile.instance()
	left_proj.speed = projectile_speed
	left_proj.damage = projectile_damage
	left_proj.position = global_position
	left_proj.direction = first_direction
	left_proj.excluded_groups = excluded_groups
	left_proj.lifetime = projectile_lifetime
	left_proj.homing_factor = homing_factor
	Game.add_root_child(left_proj)
	
	var right_proj = projectile.instance()
	right_proj.speed = projectile_speed
	right_proj.damage = projectile_damage
	right_proj.position = global_position
	right_proj.direction = second_direction
	right_proj.excluded_groups = excluded_groups
	right_proj.lifetime = projectile_lifetime
	right_proj.homing_factor = homing_factor
	Game.add_root_child(right_proj)