extends KinematicBody2D

func take_damage(amount, source):
	$DamageTaken.text = str(amount)
	$DamageTaken.rect_position = Vector2(
		-($DamageTaken.rect_size.x/2),
		$DamageTaken.rect_position.y
	)
