extends Position2D

export(Vector2) var direction = Vector2(0, -1)
export(PackedScene) var projectile = null
export(float) var projectile_speed = null
export(float) var projectile_lifetime = null
export(int) var projectile_damage = null
export(Array, String) var excluded_groups = []
export(bool) var use_fire_animation = true
export(float) var homing_factor = 0
export(bool) var use_sfx = true
export(bool) var rotate_projectiles = true
export(int, "Player", "Enemy", "Boss") var source_type = 1

func fire():
	if use_sfx: $Laser.play()
	Game.apply_screenshake(0.05, 0.2)
	var proj = projectile.instance()
	if projectile_speed != null: proj.speed = projectile_speed
	if projectile_damage != null: proj.damage = projectile_damage
	proj.position = global_position
	if projectile_lifetime != null: proj.lifetime = projectile_lifetime
	proj.direction = direction
	proj.excluded_groups = excluded_groups
	proj.homing_factor = homing_factor
	proj.source_type = source_type
	if rotate_projectiles:
		proj.rotation = direction.angle() + proj.get_projectile_rotation_offset()
	Game.add_root_child(proj)
	if use_fire_animation: $Animation.play("fire")