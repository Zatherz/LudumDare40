extends Node2D

func next_wave():
	return
	var scenes = []
	if Game.difficulty < 5:
		$SpawnTimer.wait_time = 0.5
		for i in range(0, 5): scenes.append(Game.ENEMIES.DUMBO)
	elif Game.difficulty < 10:
		for i in range(0, 10): scenes.append(Game.ENEMIES.DUMBO)
	elif Game.difficulty < 20:
		$SpawnTimer.wait_time = 1
		for i in range(0, 10): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 3): scenes.append(Game.ENEMIES.SUISQUID)
	elif Game.difficulty < 30:
		for i in range(0, 30): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 8): scenes.append(Game.ENEMIES.SUISQUID)
	elif Game.difficulty < 50:
		$SpawnTimer.wait_time = 2
		for i in range(0, 15): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 1): scenes.append(Game.ENEMIES.UGABUGA)
	elif Game.difficulty < 80:
		for i in range(0, 5): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 4): scenes.append(Game.ENEMIES.SUISQUID)
		for i in range(0, 2): scenes.append(Game.ENEMIES.UGABUGA)
	elif Game.difficulty < 110:
		for i in range(0, 8): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 1): scenes.append(Game.ENEMIES.PIRATESHIP)
	elif Game.difficulty < 140:
		for i in range(0, 8): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 4): scenes.append(Game.ENEMIES.SUISQUID)
		for i in range(0, 1): scenes.append(Game.ENEMIES.PIRATESHIP)
	elif Game.difficulty < 180:
		for i in range(0, 4): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 1): scenes.append(Game.ENEMIES.PIRATESHIP)
		for i in range(0, 2): scenes.append(Game.ENEMIES.UGABUGA)
	elif Game.difficulty < 240:
		for i in range(0, 4): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 1): scenes.append(Game.ENEMIES.BEATLE)
	elif Game.difficulty < 280:
		for i in range(0, 4): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 1): scenes.append(Game.ENEMIES.BEATLE)
		for i in range(0, 4): scenes.append(Game.ENEMIES.SUISQUID)
	elif Game.difficulty < 350:
		for i in range(0, 4): scenes.append(Game.ENEMIES.DUMBO)
		for i in range(0, 2): scenes.append(Game.ENEMIES.BEATLE)
	else:
		var i = randi() % 6
		if i == 0:
			for i in range(0, 20): scenes.append(Game.ENEMIES.DUMBO)
		elif i == 1:
			for i in range(0, 30): scenes.append(Game.ENEMIES.DUMBO)
			for i in range(0, 8): scenes.append(Game.ENEMIES.SUISQUID)
		elif i == 2:
			for i in range(0, 15): scenes.append(Game.ENEMIES.SUISQUID)
		elif i == 3:
			for i in range(0, 5): scenes.append(Game.ENEMIES.DUMBO)
			for i in range(0, 4): scenes.append(Game.ENEMIES.SUISQUID)
			for i in range(0, 2): scenes.append(Game.ENEMIES.UGABUGA)
		elif i == 4:
			for i in range(0, 4): scenes.append(Game.ENEMIES.DUMBO)
			for i in range(0, 1): scenes.append(Game.ENEMIES.PIRATESHIP)
			for i in range(0, 2): scenes.append(Game.ENEMIES.UGABUGA)
		elif i == 5:
			for i in range(0, 4): scenes.append(Game.ENEMIES.DUMBO)
			for i in range(0, 2): scenes.append(Game.ENEMIES.BEATLE)
	for s in scenes:
		var i = s.instance()
		var x = floor(rand_range($SummonPoints/TopLeft.global_position.x, $SummonPoints/TopRight.global_position.x))
		var y = floor(rand_range($SummonPoints/TopLeft.global_position.y, $SummonPoints/Bottom.global_position.y))
		i.position = Vector2(x, y)
		Game.add_root_child(i)
	
func _ready():
	call_deferred("next_wave")

func _process(delta):
	var enemies = get_tree().get_nodes_in_group("evil")
	if enemies.size() == 0 and $SpawnTimer.is_stopped():
		$SpawnTimer.start()
	
	if Input.is_action_just_pressed("ui_page_up"):
		Game.increase_difficulty()
	elif Input.is_action_just_pressed("ui_page_down"):
		for i in range(0, 9999):
			var pickup = Game.random_pickup().instance()
			pickup.good_effect(Game.get_player())
			pickup.bad_effect(Game.get_player())
	elif Input.is_action_pressed("debug_spawn_random_pickup"):
		var crate = Game.random_crate().instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	if Input.is_action_just_pressed("debug_spawn_dumbo"):
		var crate = Game.ENEMIES.DUMBO.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_ugabuga"):
		var crate = Game.ENEMIES.UGABUGA.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_suisquid"):
		var crate = Game.ENEMIES.SUISQUID.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_pirateship"):
		var crate = Game.ENEMIES.PIRATESHIP.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)
	elif Input.is_action_just_pressed("debug_spawn_beatle"):
		var crate = Game.ENEMIES.BEATLE.instance()
		crate.position = get_global_mouse_position()
		Game.add_root_child(crate)

func _on_SpawnTimer_timeout():
	$SpawnTimer.stop()
	next_wave()
