extends Area2D

export(int) var health = 2
export(int) var explosion_damage = 3
export(float) var speed = 2
export(Vector2) var force_multiplier = Vector2(1, 4)
export(float) var minimum_y_velocity = 4
export(float) var screenshake = 3
export(float) var pickup_drop_chance = 1

func maybe_drop_pickup():
	if pickup_drop_chance != 0 and randf() < pickup_drop_chance:
		var pickup = Game.random_pickup()
		var inst = pickup.instance()
		inst.position = global_position
		Game.add_root_child(inst)

var radius_marker_color_alpha = 0

func die():
	$Tween.stop(self)
	radius_marker_color_alpha = 0.3
	update()
	if not $Animation.is_playing() or $Animation.get_current_animation() != "die":
		var radius = $ExplosionArea/Shape.shape.radius
		Game.explode_scaled(global_position, 0.5, Vector2(1, 1) * radius/40)
		Game.apply_screenshake(0.5, screenshake)
		$Animation.play("die")

##damageable
func take_damage(amount, source):
	health -= amount
	if health <= 0: die()
	else: $Animation.play("damage")

var tween_rising = true
func do_tween():
	var start = 0
	var end = 0.1
	if not tween_rising:
		start = 0.1
		end = 0
	
	$Tween.interpolate_property(
		self,
		"radius_marker_color_alpha",
		start,
		end,
		1,
		Tween.TRANS_CUBIC,
		Tween.EASE_IN_OUT
	)
	$Tween.start()
	
	tween_rising = not tween_rising

func _ready():
	do_tween()
	$ExplosionArea/Shape.shape = $ExplosionArea/Shape.shape.duplicate()
	
func _draw():
	draw_circle(Vector2(0, 0), $ExplosionArea/Shape.shape.radius, Color(1, 0, 0, radius_marker_color_alpha))
	

func _physics_process(delta):
	if global_position.y > get_viewport_rect().size.y + Game.sprite_height($Sprite):
		queue_free()
	
	update()
	
	var player = Game.get_player()
	if player:
		var dir = (player.global_position - global_position).normalized()
		var final = dir * speed * force_multiplier
		if final.y < minimum_y_velocity:
			final.y = minimum_y_velocity
		position += final

func increase_explosion_radius(val):
	$ExplosionArea/Shape.shape.radius += val

func chain_react(suisquid):
	var shape = suisquid.get_node("ExplosionArea/Shape")
	shape.shape.radius = min(shape.shape.radius + 5, 100)
	suisquid.screenshake = screenshake + 0.1
	suisquid.die()

func go_out_with_a_bang():
	maybe_drop_pickup()
	for body in $ExplosionArea.get_overlapping_bodies():
		if body.is_in_group("player"):
			body.take_damage(explosion_damage, self)
		elif body.is_in_group("enemy:suisquid"):
			chain_react(body)
	for body in $ExplosionArea.get_overlapping_areas():
		if body.is_in_group("player"):
			body.take_damage(explosion_damage, self)
		elif body.is_in_group("enemy:suisquid"):
			chain_react(body)
	queue_free()

func _on_tween_completed(object, key):
	if key == ":radius_marker_color_alpha": do_tween()

func _body_entered(body):
	if body.is_in_group("player"): die()

func _body_entered_explosion_area(body):
	if body.is_in_group("player"): go_out_with_a_bang()
