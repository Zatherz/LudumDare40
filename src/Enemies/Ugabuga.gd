extends KinematicBody2D

export(int) var max_health = 10
var health
export(float) var speed = 5
var switch_order = false

onready var is_ready = true

var use_mouth_turrets = false
var anticheese_enabled = true

const COLLISION_DAMAGE = 5

enum TURRET_USED {LEFT_TURRET, MOUTH_TURRETS, RIGHT_TURRET}
var turret_used = LEFT_TURRET

func next_turret():
	if switch_order:
		if turret_used == RIGHT_TURRET:
			turret_used = MOUTH_TURRETS
		elif turret_used == MOUTH_TURRETS:
			turret_used = LEFT_TURRET
		else:
			turret_used = RIGHT_TURRET
	else:
		if turret_used == LEFT_TURRET:
			turret_used = MOUTH_TURRETS
		elif turret_used == MOUTH_TURRETS:
			turret_used = RIGHT_TURRET
		else:
			turret_used = LEFT_TURRET

func maybe_drop_crate():
	if randf() < 0.8:
		for i in range(0, floor(rand_range(1, 4))):
			var crate = Game.random_crate()
			var inst = crate.instance()
			inst.position = global_position + Vector2(
				floor(rand_range(-50, 51)),
				floor(rand_range(-50, 51))
			)
			Game.add_root_child(inst)

func set_damage_in_all_turrets(dmg):
	$LeftTurret.projectile_damage = dmg
	$RightTurret.projectile_damage = dmg
	$MouthTurrets/LeftTurret.projectile_damage = dmg
	$MouthTurrets/MiddleTurret.projectile_damage = dmg
	$MouthTurrets/RightTurret.projectile_damage = dmg
	
func set_homing_factor_in_all_turrets(homing):
	$LeftTurret.homing_factor = homing
	$RightTurret.homing_factor = homing
	$MouthTurrets/LeftTurret.homing_factor = homing
	$MouthTurrets/MiddleTurret.homing_factor = homing
	$MouthTurrets/RightTurret.homing_factor = homing

func _ready():
	if switch_order: turret_used = RIGHT_TURRET
	health = max_health
	
	$FireDelay.autostart = true
	$FireDelay.start()

func _physics_process(delta):
	var player = Game.get_player()
	if player:
		move_and_collide(Vector2((player.position - position).normalized().x * speed, 0))

##damaging
func get_contact_damage(): return COLLISION_DAMAGE

##damageable
func take_damage(amount, source):
	health -= amount
	if health <= 0:
		if not $Animation.is_playing() or $Animation.get_current_animation() != "die":
			GlobalAudio.DEATH.play()
			$Animation.play("die")
	else: $Animation.play("damage")
	
func spawn_dumbos():
	var dumbo1 = Game.ENEMIES.DUMBO.instance()
	var dumbo2 = Game.ENEMIES.DUMBO.instance()
	
	dumbo1.position = $LeftTurret.global_position
	dumbo2.position = $RightTurret.global_position
	
	Game.add_root_child(dumbo1)
	Game.add_root_child(dumbo2)

func _on_FireDelay_timeout():
	if turret_used == LEFT_TURRET:
		$LeftTurret.fire()
	elif turret_used == RIGHT_TURRET:
		$RightTurret.fire()
	else:
		$MouthTurrets/LeftTurret.fire()
		$MouthTurrets/MiddleTurret.fire()
		$MouthTurrets/RightTurret.fire()

func _on_OpeningTimer_timeout():
	$FireDelay.start()
	$FireTimer.start()
	$OpeningTimer.stop()
	next_turret()

func _on_FireTimer_timeout():
	$FireDelay.stop()
	$FireTimer.stop()
	$OpeningTimer.start()

func _switch_turrets(): pass
