extends KinematicBody2D

export(float) var speed = 200
export(int) var health = 8
export(Vector2) var direction = Vector2(1, 0)

onready var turret = $LeftTurret

##damageable
func take_damage(amount, source):
	health -= amount
	if health <= 0: $Animation.play("die")
	else: $Animation.play("damage")

func _physics_process(delta):
	var sprite_width = Game.sprite_width($Sprite)
	move_and_slide(direction * speed)
	if global_position.x <= sprite_width/2 or global_position.x >= get_viewport_rect().size.x - sprite_width/2:
		direction = -direction
		return

func explode():
	Game.explode(global_position, 0.5)
	queue_free()

func _on_FireInterval_timeout():
	turret.fire()
	if turret == $LeftTurret: turret = $RightTurret
	else: turret = $LeftTurret
