extends KinematicBody2D

export(float) var health = 2
export(float) var vert_speed = 2
export(float) var horiz_speed = 3
export(float) var pickup_drop_chance = 1
export(float) var crate_chance = 0.5
export(int) var target_y_level = null
var forced_pickup_disable_bad_effect
var forced_pickup_drop_chance
var forced_pickup_object
var sleep_until_y = null
var sleep_until_target_y_level = true
var reached_target_y_level = false

enum MODE {PEACEFUL, HOSTILE}
var mode = PEACEFUL

enum MOVEMENT_MODE {VERTICAL, HORIZONTAL}
var movement_mode = VERTICAL

var ignore_next_area_enter = false
var can_fire = true
var next_anim = "swoop"
var spawned_with_1_difficulty = false

func is_sleeping():
	if sleep_until_y: return position.y <= sleep_until_y
	if sleep_until_target_y_level and not reached_target_y_level: return true
	return false

func maybe_drop_pickup():
	var rand = randf()
	if pickup_drop_chance != 0 and rand <= pickup_drop_chance:
		var pickup = forced_pickup_object
		if pickup == null:
			if randf() <= crate_chance:
				pickup = Game.random_crate()
			else:
				pickup = Game.random_pickup()
		var inst = pickup.instance()
		inst.disable_bad_effect = forced_pickup_disable_bad_effect
		inst.position = global_position
		Game.add_root_child(inst)
		
func _ready():
	if forced_pickup_drop_chance != null:
		pickup_drop_chance = forced_pickup_drop_chance
	$Animation.play(next_anim)
	if target_y_level == null:
		target_y_level = rand_range($Sprite.texture.get_width() * $Sprite.scale.x, get_viewport_rect().size.y/2 - $Sprite.texture.get_height() * $Sprite.scale.y)

func schedule_next_anim(name):
	if not $Animation.is_playing():
		$Animation.play(name)
	else: next_anim = name

##damaging
func get_contact_damage(): return 3

func die():
	if not $Animation.is_playing() or $Animation.get_current_animation() != "die":
			GlobalAudio.DEATH.play()
			$Animation.play("die")

##damageable
func take_damage(amount, source):
	health -= amount
	if health <= 0: die()
	else: $Animation.play("damaged")

func _physics_process(delta):
	var viewport_size = get_viewport_rect().size
	if mode == PEACEFUL:
		if movement_mode == VERTICAL:
			move_and_collide(Vector2(0, vert_speed))
			if global_position.y >= viewport_size.y + $Sprite.texture.get_height() * $Sprite.scale.y:
				die()
			if position.y >= target_y_level:
				reached_target_y_level = true
				movement_mode = HORIZONTAL
		elif movement_mode == HORIZONTAL:
			var player = Game.get_player()
			if player:
				move_and_collide(Vector2((player.position - position).normalized().x * horiz_speed, 0))
				
				#if global_position.x <= $Sprite.texture.get_width() * $Sprite.scale.x:
				#	horiz_movement_multiplier = -horiz_movement_multiplier
				#if global_position.x >= viewport_size.x - $Sprite.texture.get_width() * $Sprite.scale.x:
				#	horiz_movement_multiplier = -horiz_movement_multiplier
	elif mode == HOSTILE:
		if can_fire:
			schedule_next_anim("fire")
			$Turret.fire()
			$FireDelay.start()
			can_fire = false
		else: schedule_next_anim("swoop")

func _body_detected_below(body):
	if body.is_in_group("player") and not is_sleeping():
		mode = HOSTILE

func _body_below_left(body):
	if body.is_in_group("player") and not is_sleeping():
		mode = PEACEFUL

func _on_FireDelay_timeout():
	can_fire = true

func _on_animation_finished(name):
	if name != "idle": $Animation.play(next_anim)