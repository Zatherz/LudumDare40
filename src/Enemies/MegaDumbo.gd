extends KinematicBody2D

export(float) var speed = 100
export(bool) var fake_damage = false
export(bool) var frozen = false
var health

signal in_position
var is_ready = false

export(float) var lazer_activation_distance = 250
export(Color) var lazer_activation_area_color = Color(1, 0, 0, 0.5)
export(int) var max_number_of_dumbos_on_screen = 5
export(float) var dumbo_helpers_drop_chance = 0.3
export(float) var chance_to_summon_suisquid = 0.3
var sleep_until_y

enum PATTERN {IDLE, EYE_LASERS, TENTACLE_SPAM}
var shooting = true

##boss
export(int) var max_health = 100
func get_max_health(): return max_health
func get_health(): return health

func final_explosion():
	Game.apply_screenshake(1, 15)
	Game.explode_scaled(global_position, 0.5, Vector2(3, 3))

func random_cinematic_explosion():
	for i in range(0, 2):
		var x = rand_range(-$Sprite.texture.get_width()/2 * $Sprite.scale.x, $Sprite.texture.get_width()/2 * $Sprite.scale.y)
		var y = rand_range(-$Sprite.texture.get_height()/2 * $Sprite.scale.y, $Sprite.texture.get_height()/2 * $Sprite.scale.y)
		
		var pos = Vector2(global_position.x + x, global_position.y + y)
		Game.explode(pos, 0.6)
	Game.apply_screenshake(0.2, 5)

func _draw():
	draw_circle(Vector2(), lazer_activation_distance, lazer_activation_area_color)

func get_pattern():
	var player = Game.get_player()
	if player and not is_sleeping():
		if player.global_position.distance_to(global_position) <= lazer_activation_distance:
			return EYE_LASERS
		if shooting: return TENTACLE_SPAM
		return IDLE
	return IDLE

func fire_turret(turret, target):
	turret.direction = (target - turret.global_position).normalized()
	turret.fire()

func fire(target):
	fire_turret($Turret1, target)
	fire_turret($Turret2, target)
	fire_turret($Turret3, target)
	fire_turret($Turret4, target)
	fire_turret($Turret5, target)
	fire_turret($Turret6, target)
	fire_turret($Turret7, target)
	fire_turret($Turret8, target)

var turret_idx = 1
func fire_one_at_a_time(target):
	fire_turret(get_node(str("Turret", turret_idx)), target)
	turret_idx += 1
	if turret_idx > 8: turret_idx = 1

func _ready():
	health = max_health
	if sleep_until_y == null:
		sleep_until_y = get_viewport_rect().size.y/2

func is_sleeping():
	if sleep_until_y != null: return position.y < sleep_until_y
	return false
	
func is_invulnerable_to(source):
	return is_sleeping() or not source.is_in_group("enemy:megadumbo/projectile")

##damaging
func get_contact_damage():
	return 10

##damageable
func take_damage(amount, source):
	if fake_damage:
		$Animation.play("damage")
		return
	if is_invulnerable_to(source):
		$Animation.play("faildamage")
		return
	health -= amount
	if health <= 0:
		frozen = true
		$Animation.play("die")
	else: $Animation.play("damage")

func _physics_process(delta):
	$HPLabel.text = str(health, " HP ")
	$HPLabel.rect_position.x = -$HPLabel.rect_size.x/2
	
	if is_sleeping():
		move_and_slide(Vector2(0, speed))
	else:
		if not is_ready:
			is_ready = true
			emit_signal("in_position")
		if $DumboSpawnTimer.is_stopped():
			$DumboSpawnTimer.start()

func fire_beam_from(pos):
	var proj = Game.PROJECTILES.BEAM.instance()
	proj.excluded_groups = ["evil"]
	proj.origin = weakref(pos)
	proj.direction = (Game.get_player().global_position - pos.global_position).normalized()
	proj.rotation = proj.direction.angle()
	proj.lifetime = 0.30
	proj.speed = 40
	proj.damage = 1
	Game.spawn(proj, pos)

func fire_beam():
	$BeamSound.play()
	fire_beam_from($LeftEye)
	fire_beam_from($RightEye)

func fire_octo():
	fire_one_at_a_time(Game.get_player().global_position)

func _on_SalvoTimer_timeout():
	if frozen: return
	if get_pattern() == TENTACLE_SPAM: fire_octo()

func _on_LaserTimer_timeout():
	if frozen: return
	if get_pattern() == EYE_LASERS: fire_beam()

func _on_DumboSpawnTimer_timeout():
	if frozen: return
	
	print("spawndumbo")
	var count = get_tree().get_nodes_in_group("enemy:dumbo").size()
	if count >= max_number_of_dumbos_on_screen: return
	var inst
	
	if randf() < chance_to_summon_suisquid:
		inst = Game.ENEMIES.SUISQUID.instance()
	else:
		inst = Game.ENEMIES.DUMBO.instance()
		inst.forced_pickup_object = Game.SHIELD_REGEN_GEM
		inst.forced_pickup_drop_chance = dumbo_helpers_drop_chance
		inst.forced_pickup_disable_bad_effect = true
	
	var sprite_width = Game.sprite_width(inst.get_node("Sprite"))
	var x = rand_range(sprite_width/2, global_position.x - Game.sprite_width($Sprite)/2 - sprite_width/2)
	Game.spawn(inst, Vector2(x, -100))

func _on_SalvoToggleTimer_timeout():
	if frozen: return
	shooting = not shooting