extends KinematicBody2D

enum PHASE {WITH_PILOT, ALONE, DEAD}
var phase = WITH_PILOT

var firing = false
var pilot_health
var ship_health
export(float) var speed = 100
export(int) var bullets_for_rockets_to_explode_into = 8
export(float) var rocket_speed = 2
export(float) var rocket_speed_once_activated = 8
export(int) var rocket_damage = 5
export(int) var angry_health_boundary = 25
export(int) var contact_damage = 8

##boss
export(int) var pilot_max_health = 50
export(int) var ship_max_health = 100
func get_max_health():
	if phase == WITH_PILOT: return pilot_max_health
	if phase == ALONE: return ship_max_health
func get_health():
	if phase == WITH_PILOT: return pilot_health
	if phase == ALONE: return ship_health

var angry = false
var second_phase_speed_multiplier = 1.5
var moving = true
var current_crosshair = null

func _ready():
	pilot_health = pilot_max_health
	ship_health = ship_max_health

func become_angry():
	angry = true
	$PilotFireInterval.start()
	$TimeBetweenCrosshairs.wait_time /= 2
	bullets_for_rockets_to_explode_into = 12

func final_explosion():
	Game.apply_screenshake(1, 15)
	Game.explode_scaled(global_position, 0.5, Vector2(3, 3))

func random_cinematic_explosion():
	for i in range(0, 2):
		var x = rand_range(-$ShipSprite.texture.get_width()/2 * $ShipSprite.scale.x, $ShipSprite.texture.get_width()/2 * $ShipSprite.scale.y)
		var y = rand_range(-$ShipSprite.texture.get_height()/2 * $ShipSprite.scale.y, $ShipSprite.texture.get_height()/2 * $ShipSprite.scale.y)
		
		var pos = Vector2(global_position.x + x, global_position.y + y)
		Game.explode(pos, 0.6)
	Game.apply_screenshake(0.2, 5)

##damageable
func take_damage(amount, source):
	if phase == WITH_PILOT:
		if not $Animation.is_playing(): $Animation.play("pilot_damage")
		pilot_health -= amount
		if pilot_health <= angry_health_boundary and not angry:
			become_angry()
		if pilot_health <= 0:
			switch_to_alone_phase()
	elif phase == ALONE:
		if not $Animation.is_playing(): $Animation.play("ship_damage")
		ship_health -= amount
		if ship_health <= 0:
			$Animation.play("die")
			$TimeBetweenCrosshairs.stop()
			phase = DEAD
	
	
func switch_to_alone_phase():
	Game.kill_everything_except_bosses()
	phase = ALONE
	rocket_speed_once_activated *= 2
	rocket_speed *= 2
	bullets_for_rockets_to_explode_into = 8
	firing = false
	$SalvoInterval.stop()
	$TimeUntilStrike.stop()
	if current_crosshair != null and current_crosshair.get_ref() != null:
		current_crosshair.get_ref().start_decay()
	$ShipCollisionShape.disabled = false
	$PilotCollisionShape.disabled = true
	$TimeBetweenCrosshairs.wait_time *= 2
	Game.explode($PilotSprite.global_position, 0.5)
	speed *= second_phase_speed_multiplier
	$PilotSprite.hide()

const CROSSHAIR = preload("res://scenes/PirateShipCrosshair.tscn")

const FRACTION_OF_SCREEN_ALLOWED_TO_MOVE_IN = 1/3.0

func _physics_process(delta):	
	var player = Game.get_player()
	if player and (current_crosshair == null or current_crosshair.get_ref() == null):
		var dir = (player.global_position - global_position).normalized()
		var final = dir * speed
		var sim = global_position + final
		if phase == WITH_PILOT and sim.y >= (get_viewport_rect().size.y * FRACTION_OF_SCREEN_ALLOWED_TO_MOVE_IN):
			final.y = 0
		move_and_slide(final)
	
func _crosshair_time():
	var player = Game.get_player()
	
	if not player: return
	if current_crosshair != null and current_crosshair.get_ref() != null: return
	
	var crosshair = CROSSHAIR.instance()
	crosshair.position = player.global_position
	Game.add_root_child(crosshair)
	
	if phase == WITH_PILOT:
		current_crosshair = weakref(crosshair)
		$TimeUntilStrike.start()
	else:
		strike(weakref(crosshair))

func strike(crosshair):
	var rocket = Game.PROJECTILES.ROCKET.instance()
	rocket.target = crosshair
	rocket.speed_once_activated = 30
	rocket.lifetime = 5
	rocket.excluded_groups = ["evil"]
	rocket.position = $RocketOrigin.global_position
	rocket.direction = Vector2(0, 1)
	rocket.speed = rocket_speed
	rocket.speed_once_activated = rocket_speed_once_activated
	rocket.rotation = PI
	rocket.bullets_to_explode_into = bullets_for_rockets_to_explode_into
	rocket.damage = rocket_damage
	Game.add_root_child(rocket)

func _time_to_strike():
	$Launch.play()
	strike(current_crosshair)
	$TimeUntilStrike.stop()

func _rocket_time():
	var player = Game.get_player()
	if player == null: return
	
	$Launch.play()
	var rocket = Game.PROJECTILES.ROCKET.instance()
	rocket.target = weakref(player)
	rocket.speed_once_activated = 30
	rocket.lifetime = 5
	rocket.excluded_nodes = [self]
	rocket.position = $RocketOrigin.global_position
	rocket.direction = Vector2(0, 1)
	rocket.speed = rocket_speed
	rocket.speed_once_activated = rocket_speed_once_activated
	rocket.rotation = PI
	rocket.bullets_to_explode_into = bullets_for_rockets_to_explode_into * 2
	rocket.damage = rocket_damage
	Game.add_root_child(rocket)

func _on_PilotFireInterval_timeout():
	if firing: $PilotTurret.fire()

func _on_SalvoInterval_timeout():
	firing = not firing

func _body_collided(body):
	if body.is_in_group("player"): body.take_damage(contact_damage, self)
