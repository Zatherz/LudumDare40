extends KinematicBody2D

export(int) var beam_width = 10
export(bool) var counterclockwise = true
export(int) var health = 15
export(float) var speed = 70
export(float) var wait_until_activation = 2
var enabled = false
var waited_time = 0

##damageable
func take_damage(amount, source):
	health -= amount
	if health <= 0: 
		if not $Animation.is_playing() or $Animation.get_current_animation() != "die": $Animation.play("die")
	else: $Animation.play("damage")
	
##damaging
func get_contact_damage(): return 5
	
func _physics_process(delta):
	waited_time += delta
	if waited_time >= wait_until_activation:
		enabled = true
		
	var player = Game.get_player()
	if player:
		var dir = (player.global_position - global_position).normalized()
		move_and_slide(dir * speed)
	
func _ready():
	if counterclockwise: i = 107

var i = 0
func _on_Timer_timeout():
	if not enabled: return
	#if randf() < 0.2: counterclockwise = not counterclockwise
	
	if counterclockwise:
		i -= 1
		if i < 0: i = 7
	else:
		i += 1
		if i > 8: i = 0
	
	var rad = i * PI /4
	var dir = Vector2(sin(rad), cos(rad))
	
	$BeamSound.play()
	var beam = Game.PROJECTILES.BEAM.instance()
	beam.origin = weakref(self)
	beam.position = global_position
	beam.direction = dir
	beam.excluded_groups = ["evil"]
	beam.lifetime = 1
	beam.speed = 50
	beam.color = Color(
		0.9882352941176471,
		0.6470588235294118,
		0.203921568627451
	)
	beam.thickness = 50
	beam.piercing = true
	beam.damage = 1
	Game.apply_screenshake(0.3, 0.5)
	Game.add_root_child(beam)