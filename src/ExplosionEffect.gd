extends Sprite

export(float) var length = 1
export(bool) var audio = true
var exploded = false

func _ready():
	if audio: $Sound.play()
	$Tween.interpolate_property(
		self,
		"frame",
		0,
		7,
		length,
		Tween.TRANS_CUBIC,
		Tween.EASE_IN_OUT
	)
	$Tween.start()

func _on_tween_completed(object, key):
	exploded = true
	hide()
	
func _process(delta):
	if exploded and not $Sound.playing and not is_queued_for_deletion():
		queue_free()

func _on_audio_finished():
	print("audio finished") #???
	queue_free()
