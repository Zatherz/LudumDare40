extends Area2D

func _ready():
	$Sprite.modulate.a = 1

func start_decay():
	$Animation.play("decay")

func _emergency_destroy():
	start_decay()
