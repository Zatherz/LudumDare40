extends Node

export(int) var index = -1
var wave setget get_wave, set_wave

func get_wave(): return Game.wave
func set_wave(val): print("ERROR: Can't set wave directly - use end_wave() and start_wave()")

func end_wave():
	Game.end_wave()
	print("END Wave ", Game.wave)
	pre_wave(Game.wave)
func start_wave():
	Game.start_wave()
	print("BEGIN Wave ", Game.wave)
	wave(Game.wave)
func end(): Game.level_ended()

func all_enemies_are_dead(wave): pass
func pre_wave(last_wave): pass
func wave(wave): pass

func _ready():
	if has_node("Music"): get_node("Music").play()

func _check_enemy_count():
	if Game.get_enemy_count() == 0:
		print("All enemies are dead")
		all_enemies_are_dead(Game.wave)
