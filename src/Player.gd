extends Area2D

export(float) var speed = 6
export(int) var max_health = 30
export(bool) var invincible = false
export(int) var max_shield_juice = 3
export(bool) var can_move = true
var shield_juice
var health
var shielding

const CAPS = {
	SHOT_SPEED = 40,
	DAMAGE = 99999
}

func set_shot_speed_of_both_turrets(val):
	$LeftTurret.projectile_speed = min(CAPS.SHOT_SPEED, val)
	$RightTurret.projectile_speed = min(CAPS.SHOT_SPEED, val)
	
func increase_shot_speed_of_both_turrets(val):
	$LeftTurret.projectile_speed = min(CAPS.SHOT_SPEED, $LeftTurret.projectile_speed + val)
	$RightTurret.projectile_speed = min(CAPS.SHOT_SPEED, $RightTurret.projectile_speed + val)
	
func set_damage_of_both_turrets(val):
	$LeftTurret.projectile_damage = min(CAPS.DAMAGE, val)
	$RightTurret.projectile_damage = min(CAPS.DAMAGE, val)
	
func increase_damage_of_both_turrets(val):
	$LeftTurret.projectile_damage = min(CAPS.DAMAGE, $LeftTurret.projectile_damage + val)
	$RightTurret.projectile_damage = min(CAPS.DAMAGE, $RightTurret.projectile_damage + val)
	
func set_projectile_in_both_turrets(node):
	$LeftTurret.projectile = node
	$RightTurret.projectile = node

var turret
onready var TURRET = {
	LEFT  = get_node("LeftTurret"),
	RIGHT = get_node("RightTurret")
}

var can_fire = true

func die():
	Game.explode(global_position, 0.5)
	Game.game_over()
	queue_free()
	
func pickup_decrease_health(amount, source):
	health -= amount
	Game.last_source_of_damage = Game.DamageSource.create_from(source)
	if health <= 0: die()

##damageable
func take_damage(amount, source):
	if shielding and not source.is_in_group("projectile_pierces_shield"):
		return
	if not $Animation.is_playing(): $Animation.play("damaged")
	if invincible: return
	print("DMG: ", source)
	Game.last_source_of_damage = Game.DamageSource.create_from(source)
	health -= amount
	if health <= 0:
		die()

func _ready():
	Game.last_source_of_damage = null
	health = max_health
	turret = TURRET.LEFT
	shield_juice = max_shield_juice
	
func fix_move_direction(dir):
	var viewport_size = get_viewport_rect().size
	
	if dir.x < 0:
		if not global_position.x > $Sprite.texture.get_width(): dir.x = 0
	elif dir.x > 0:
		if not global_position.x < viewport_size.x - $Sprite.texture.get_width(): dir.x = 0
	
	if dir.y < 0:
		if not global_position.y > $Sprite.texture.get_height(): dir.y = 0
	elif dir.y > 0:
		if not global_position.y <  viewport_size.y - $Sprite.texture.get_height(): dir.y = 0
	
	return dir

func shield(sec):
	if shield_juice <= 0:
		shielding = false
		shield_juice = 0
		return
	shield_juice -= sec
	shielding = true

func _physics_process(delta):
	if shielding: $Shield.show()
	else: $Shield.hide()
	
	if can_move:
		var move_dir = Vector2()
		
		if Input.is_action_pressed("up"):    move_dir.y -= 1
		if Input.is_action_pressed("down"):  move_dir.y += 1
		if Input.is_action_pressed("left"):  move_dir.x -= 1
		if Input.is_action_pressed("right"): move_dir.x += 1
		
		if Input.is_action_pressed("shield"):
			shield(delta)
		else:
			shielding = false
		
		if Input.is_action_pressed("shoot"):
			if can_fire:
				fire()
				can_fire = false
				$FireDelay.start()
	#if Input.is_action_just_released("shoot"): can_fire = true
	
		move_dir = fix_move_direction(move_dir)
		var final = move_dir * speed
		if Input.is_action_pressed("slow_down"): final *= 0.3
		position += final

func fire():
	$Fire.play()
	turret.fire()
	if turret == TURRET.LEFT: turret = TURRET.RIGHT
	else:                     turret = TURRET.LEFT

func _on_FireDelay_timeout():
	can_fire = true
	$FireDelay.stop()

func _on_impact(body):
	if body.is_in_group("projectile"): return
	if body.is_in_group("evil") and body.is_in_group("damaging"):
		take_damage(body.get_contact_damage(), body)
		
func activate_pickup(pickup):
	pickup.do_good_effect(self)
	pickup.do_bad_effect(self)
	if not $Animation.is_playing(): $Animation.play("perk")
	
func grant_ability(ability):
	var inst = ability.instance()
	inst.player = self
	print("DEBUG: Granted ability ", inst.name)
	$AbilityContainer.add_child(inst)
	
func get_abilities(): return $AbilityContainer.get_children()