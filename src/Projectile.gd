extends Area2D

export(float) var speed = 1
export(Vector2) var direction = Vector2()
export(int) var damage = 10
export(Array, String) var excluded_groups = null
export(bool) var piercing = false
export(float) var lifetime = 1.5
export(float) var homing_factor = 0
enum SOURCE_TYPE {PLAYER, ENEMY, BOSS}
export(int, "Player", "Enemy", "Boss") var source_type = 1
export(bool) var dont_destroy_when_not_on_screen = false

enum PROJECTILE_ORIENTATION {
	FACING_RIGHT,
	FACING_UP,
	FACING_LEFT,
	FACING_DOWN
}
const PROJECTILE_ORIENTATION_TO_RAD = {
	FACING_RIGHT: 0,
	FACING_UP: 90 * (PI/180),
	FACING_LEFT: 180 * (PI/180),
	FACING_DOWN: 270 * (PI/180)
}
export(int, "Facing Right", "Facing Up", "Facing Left", "Facing Down") var projectile_orientation = 0
var excluded_nodes = null

func get_projectile_rotation_offset():
	return PROJECTILE_ORIENTATION_TO_RAD[projectile_orientation]

func _ready():
	if not is_in_group("projectile"): add_to_group("projectile")
	
	$Tween.interpolate_property(
		$Sprite,
		"modulate",
		Color(1, 1, 1, 1),
		Color(1, 1, 1, 0),
		lifetime,
		Tween.TRANS_SINE,
		Tween.EASE_OUT
	)
	$Tween.interpolate_property(
		self,
		"damage",
		damage,
		1,
		lifetime,
		Tween.TRANS_SINE,
		Tween.EASE_OUT
	)
	$Tween.start()

const VELOCITY_TOLERANCE = 0.05
func barely_moving(final):
	return ((
		(final.x >= 0 and final.x < VELOCITY_TOLERANCE) or
		(final.x <= 0 and final.x > -VELOCITY_TOLERANCE)
	) and (
		(final.y >= 0 and final.y < VELOCITY_TOLERANCE) or
		(final.y <= 0 and final.y > -VELOCITY_TOLERANCE)
	))
	
func outside_of_screen(final):
	var sprite_width = Game.sprite_width($Sprite)
	var sprite_height = Game.sprite_height($Sprite)
	return (
		(final.x <= -sprite_width/2 or final.x >= get_viewport_rect().size.x + sprite_width/2) or
		(final.y <= -sprite_height/2 or final.y >= get_viewport_rect().size.y + sprite_height/2)
	)

func _physics_process(delta):
	var final = direction * speed
	if homing_factor != 0:
		var player = Game.get_player()
		if player:
			var dir = (player.global_position - global_position).normalized() * homing_factor
			final += dir * speed
	position += final
	if $Sprite.modulate.a == 0 or barely_moving(final) or (lifetime > 0.5 and not dont_destroy_when_not_on_screen and outside_of_screen(global_position)):
		queue_free()

func _on_impact(body):
	on_impact(body)

func on_impact(body):
	if body.is_in_group("damageable"):
		if excluded_groups:
			for group in excluded_groups:
				if body.is_in_group(group): return
		if excluded_nodes:
			for node in excluded_nodes:
				if node == body: return
		var keep = on_body_impact(body)
		if not piercing and not keep: queue_free()

const IF_ALPHA_IS_LOWER_THAN_THIS_DONT_DEAL_DAMAGE = 0.2

func on_body_impact(body):
	if $Sprite.modulate.a >= IF_ALPHA_IS_LOWER_THAN_THIS_DONT_DEAL_DAMAGE:
		Game.apply_screenshake(0.1, 0.7)
		body.take_damage(int(damage), self)