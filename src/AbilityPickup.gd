extends "res://src/Pickup.gd"

export(PackedScene) var ability

func good_effect(player):
	GlobalAudio.random_powerup().play()
	player.grant_ability(ability)